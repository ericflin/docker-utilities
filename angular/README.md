To run tests through the polymer-cli tool, use:

`docker run -ti --rm -v "$PWD":/src -w /src polymer bash -c "Xvfb :0 -ac \
+extension RANDR -screen 0 1366x768x24 & \
xvfb-run polymer test
`

To use the polymer server (perhaps to run tests in the browser), use:

`docker run -ti --rm -v "$PWD":/src -w /src -p 0.0.0.0:8123:8081 polymer \
polymer serve --hostname 0.0.0.0`
